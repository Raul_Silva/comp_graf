import pyglet

tri = pyglet.graphics.vertex_list(3, ('v3f',(-5,-5,0,5,-5,0,0,5,0)),
                                     ('c3B',(255,0,0,0,255,0,0,0,255)))

window = pyglet.window.Window()
aspect = window.width / window.height
ang = 0

@window.event
def on_draw():
    window.clear()

    pyglet.gl.glMatrixMode(pyglet.gl.GL_PROJECTION)
    pyglet.gl.glLoadIdentity()
    pyglet.gl.gluPerspective(30,aspect,0.1,2000)

    pyglet.gl.glMatrixMode(pyglet.gl.GL_MODELVIEW)
    pyglet.gl.glLoadIdentity()
    pyglet.gl.glTranslated(0,0,-30)
    pyglet.gl.glRotated(ang,0,1,0)
    tri.draw(pyglet.gl.GL_LINE_LOOP)
    pyglet.gl.glRotated(90,0,1,0)
    tri.draw(pyglet.gl.GL_LINE_LOOP)
    pyglet.gl.glTranslated(-5,0,0)
    pyglet.gl.glRotated(90,0,0,1)
    tri.draw(pyglet.gl.GL_LINE_LOOP)

@pyglet.clock.schedule
def update(dt):
    global ang
    ang = (ang + dt * 100) % 360

pyglet.app.run()